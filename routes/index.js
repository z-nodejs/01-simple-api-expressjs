const router = (app) => {
    app.get('/user/:userId', (req, res) => {
        const user = { id: req.params.userId };
        res.status(200).send(user);
    });
};

module.exports = router;